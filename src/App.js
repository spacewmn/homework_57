import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import EqualCounter from "./components/EqualCounter/EqualCounter";
import IndividualCounter from "./components/IndividualCounter/IndividualCounter";
import IndividualOrder from "./components/IndividualOrder/IndividualOrder";
import { nanoid } from 'nanoid';

const App = () => {
    const [choice, setChoice] = useState ( '');

    const makeChoice = event => {
        if (event.target.checked)
            setChoice(event.target.value)
        setShowTotal(!showTotal)
    }

  const [counter, setCounter] = useState({
    equalCounter: false,
    individualCounter: false
  });

  const [value, setValue] = useState({
      people: 0,
      order: 0,
      tip: 0,
      delivery: 0
  });

  const [valueInd, setValueInd] = useState({
    people: [{
        // id: nanoid(),
        // name: '',
        // order: ''
    }],
    tip: 0,
    delivery: 0
  });

    const [showTotal, setShowTotal] = useState(false);

    const createName = (event, id) => {
      const index = valueInd.people.findIndex(p => p.id === id);
      const copyPeople = {...valueInd};
      const copyPerson = {...copyPeople.people[index]}
      copyPerson.name = event.target.value;
      copyPeople.people[index] = copyPerson;
      setValueInd(copyPeople)
      console.log(valueInd);
  };

    const createOrder = (event, id) => {
        const index = valueInd.people.findIndex(p => p.id === id);
        const copyPeople = {...valueInd};
        const copyOrder = {...copyPeople.people[index]}
        copyOrder.order = event.target.value;
        copyPeople.people[index] = copyOrder;
        setValueInd(copyPeople)
        console.log(valueInd);
    };


    const currentValueInd = (event) => {
      const name = event.target.value;
        const order = event.target.value;
        const copyValueInd = [...valueInd.person];
        copyValueInd.name = name;
        copyValueInd.order = Number(order);
        setValueInd(copyValueInd);
        console.log(copyValueInd)
    };

  const addPerson = (event, inputtedText) => {
      event.preventDefault();
      if (inputtedText !== '') {
          const copyValueInd = {...valueInd};
          const copyPeople = [...copyValueInd.people]
          const newPerson = {
              id: nanoid(),
              name: '',
              order: ''
          };
          copyPeople.push(newPerson);
          copyValueInd.people = copyPeople;
          setValueInd(copyValueInd);
      }
      console.log(valueInd);
  };

    const removeOrder = id => {
        const index = valueInd.findIndex(p => p.index === id)
        const copyValueInd = [...valueInd];
        copyValueInd.splice(index, 1);
        setValueInd(copyValueInd);
    };


    const showTotalDiv = () => {
      setShowTotal(!showTotal);
    };

    const currentValue = (event) => {
        const name = event.target.name;
        const val = event.target.value;
        const copyValue = {...value};
        copyValue[name] = Number(val);
        setValue(copyValue);
        console.log(copyValue)
    };

    let sum = null;

    if(showTotal){
        sum = (
            <EqualCounter
                value={value}
                currentValue={currentValue}
                showTotalDiv={showTotalDiv}
                showTotal={showTotal}
            />
        )
    }
    else {
        sum = (
            <IndividualCounter
                valueInd={valueInd}
                currentValueInd={currentValueInd}
                add={(event) => addPerson(event, valueInd)}
                showTotalDiv={showTotalDiv}
                showTotal={showTotal}
            />)
    }


    return (
    <div className="App">
      <form>
        <p>Сумма заказа считается:</p>
        <input type="radio" id="equal" name="counter" value="equal" checked={value === "equal"} onChange={makeChoice}/>
        <label htmlFor="equal">Поровну между всеми участниками</label>
        <input type="radio" id="individual" name="counter" value="individual" checked={value === "individual"} onChange={makeChoice}/>
        <label htmlFor="individual">Каждому индивидуально</label>
      </form>
      <EqualCounter
          value={value}
          currentValue={currentValue}
          showTotalDiv={showTotalDiv}
          showTotal={showTotal}
      />
      <IndividualCounter
          valueInd={valueInd}
          currentValueInd={currentValueInd}
          add={(event) => addPerson(event, valueInd)}
          showTotalDiv={showTotalDiv}
          showTotal={showTotal}

      />
        <div>
            {
                valueInd.people.map((order, index) => {
                    return <IndividualOrder
                        addName={createName}
                        createOrder={createOrder}
                        id={order.id}
                        key={index}
                        name={order.name}
                        remove={() => removeOrder(index)}
                    />
                })
            }
        </div>
        {sum}
    </div>
  );
}

export default App;
