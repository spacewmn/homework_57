import React from 'react';

const IndividualOrder = props => {
    return (
        <div>
            <input type="text" placeholder="Имя клиента" onChange={(event) => props.addName(event, props.id)}/>
            <input type="number" placeholder="Сумма" min="0" onChange={(event) => props.createOrder(event, props.id)}/>
            <button type="button" onClick={props.remove}>-</button>
        </div>
    );
};

export default IndividualOrder;