import React from 'react';

const EqualCounter = (props) => {
    let totalList = null;
    if (props.showTotal) {
        const people = props.value.people;
        const order = props.value.order;
        const tip = props.value.tip;
        const delivery = props.value.delivery;
        const total = Math.ceil(order + (order * tip / 100) + delivery);
        const calculation =  Math.ceil((order + (order * tip / 100) + delivery) / people);

        console.log(total);
        console.log(people);
        console.log(calculation);

        totalList = (
            <div>
                <p>Общая сумма: {total} coм</p>
                <p>Количество человек: {people}</p>
                <p>Каждый платить по: {calculation} coм</p>
            </div>
        )
    }

    return (
        <div>
            <div>
            <label htmlFor="people">Человек: </label>
                <input type="number" name="people" id="people" min="0" onChange={props.currentValue}/>
                <span> чел.</span>
            </div>
            <div>
            <label htmlFor="order">Сумма заказа: </label>
                <input type="number" name="order" id="order" min="0" onChange={props.currentValue}/>
                <span> сом</span>
        </div>
            <div>
            <label htmlFor="tip">Процент чаевых: </label>
                <input type="number" name="tip" id="tip" min="0" onChange={props.currentValue}/>
                <span> %</span>
</div>
            <div>
            <label htmlFor="delivery">Доставка: </label>
                <input type="number" name="delivery" id="delivery" min="0" onChange={props.currentValue}/>
                <span> сом</span>
</div>
            <button type="button" onClick={props.showTotalDiv}>Рассчитать</button>
            {totalList}
        </div>
    );
};

export default EqualCounter;