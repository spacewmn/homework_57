import React from 'react';

const IndividualCounter = (props) => {
    let indivList = null;
    if (props.showTotal) {
        const people = props.valueInd.people.name;
        const order = props.valueInd.people.order;
        const tip = props.valueInd.tip;
        const delivery = props.valueInd.delivery;
        console.log(people);
        console.log(tip);
    }
    indivList = (
        <div>
            <p>Количество человек:  </p>
        </div>
    )


    return (
            <form>
                <div>
                    <input type="text" placeholder="Имя клиента"/>
                    <input type="number" placeholder="Сумма" min="0" onChange={props.currentValueInd}/>
                    <button type="button" onClick={props.remove}>-</button>
                </div>
                <button type="submit" onClick={props.add}>+</button>
                <div>
                    <label htmlFor="tip">Процент чаевых: </label>
                    <input type="number" name="tip" id="tip" min="0" onChange={props.currentValueInd}/>
                    <span> %</span>
                </div>
                <div>
                    <label htmlFor="delivery">Доставка: </label>
                    <input type="number" name="delivery" id="delivery" min="0" onChange={props.currentValueInd}/>
                    <span> сом</span>
                </div>
                <button type="button" onClick={props.showTotalDiv}>Рассчитать</button>
                {indivList}
            </form>

    );
};

export default IndividualCounter;